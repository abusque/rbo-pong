using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace rboPong
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class RboPong : Microsoft.Xna.Framework.Game
    {
        // Global Keys
        public static Keys QuitKey = Keys.Escape;
        public static Keys PauseKey = Keys.P;
        public static Keys ModeKey = Keys.F1;

        public GraphicsDeviceManager graphics { get; private set; }
        public SpriteBatch spriteBatch { get; private set; }
        public InputManager InputManager { get; private set; }
        public static Random RandomGenerator { get; private set; }
        public Scenes.SceneManager SceneManager { get; private set; }

        public Vector2 ScreenSize { get; private set; }

        public RboPong()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            RandomGenerator = new Random();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            ResourceManager<Texture2D>.content = this.Content;
            ResourceManager<Texture2D>.Load("Textures/", "Paddle");
            ResourceManager<Texture2D>.Load("Textures/", "Ball");

            ResourceManager<SpriteFont>.content = this.Content;
            ResourceManager<SpriteFont>.Load("SpriteFonts/", "ScoreFont");

            ResourceManager<SoundEffect>.content = this.Content;
            ResourceManager<SoundEffect>.Load("SoundEffects/", "beep");

            ScreenSize = new Vector2(graphics.GraphicsDevice.Viewport.Width,
                                     graphics.GraphicsDevice.Viewport.Height);

            SceneManager = Scenes.SceneManager.getInstance(this);
            Components.Add(SceneManager);            
            
            InputManager = new InputManager(this);
            Components.Add(InputManager);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Paddle.spriteBatch = spriteBatch;
            Ball.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override bool BeginDraw()
        {
            spriteBatch.Begin();
            return base.BeginDraw();
        }


        protected override void EndDraw()
        {
            spriteBatch.End();
            base.EndDraw();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            base.Draw(gameTime);
        }


        public static int RandomSign()
        {
            return Math.Sign(RandomGenerator.Next(1000) - 500);
        }
    }
}
