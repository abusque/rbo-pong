﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace rboPong.GameComponents
{
    public abstract class Match : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public const uint WINNING_SCORE = 21;

        public enum Mode { TWO_PLAYERS, HUMAN_VS_HARD_AI, HUMAN_VS_CRAZY_AI, NB_MODE }

        public RboPong PongGame { get; private set; }
        public Player Player1 { get; protected set; }
        public Player Player2 { get; protected set; }
        public Player Server { get; set; } // A reference to the player who serves the ball
        public Ball SuperBall { get; protected set; }

        public Vector2 PaddleSize { get; protected set; }
        public Vector2 SuperBallSize { get; protected set; }

        public ScoreWriter ScoreWriter { get; private set; }
        private bool ResetPositionsWhenScore { get; set; }

        public Match(RboPong game)
            : base(game)
        {
            PongGame = game;
        }

        public Match(RboPong game, bool resetPositionsWhenScore)
            : base(game)
        {
            PongGame = game;
            ResetPositionsWhenScore = resetPositionsWhenScore;
        }

        public static Match getMatch(RboPong game, Mode mode)
        {
            // TO DO GameScene polymorphism
            switch (mode)
            {
                case Mode.TWO_PLAYERS:
                    return new TwoPlayersMatch(game);
                case Mode.HUMAN_VS_HARD_AI:
                    return new HumanVsHardAIMatch(game);
                case Mode.HUMAN_VS_CRAZY_AI:
                    return new HumanVsAIMatch(game);
                default:
                    throw new NotImplementedException();
            }
        }

        public override void Initialize()
        {
            PaddleSize = new Vector2(ResourceManager<Texture2D>.get("Paddle").Width,
                                        ResourceManager<Texture2D>.get("Paddle").Height);
            SuperBallSize = new Vector2(ResourceManager<Texture2D>.get("Ball").Width,
                                           ResourceManager<Texture2D>.get("Ball").Height);
            SuperBall = new Ball(this, PongGame.ScreenSize / 2 - SuperBallSize / 2, Color.White);
            SuperBall.Initialize();

            InitializePlayers();
            Server = Player1; // By default, player 1 serves at beginning of game

            ScoreWriter = new ScoreWriter(this);
            ScoreWriter.Initialize();

        }

        protected virtual void InitializePlayers()
        {
            Player1.Initialize();
            Player2.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (Player1.Score >= WINNING_SCORE || Player2.Score >= WINNING_SCORE)
                ResetGame();

            Player1.Update(gameTime);
            Player2.Update(gameTime);
            SuperBall.Update(gameTime);
            ScoreWriter.Update(gameTime);

            base.Update(gameTime);
        }


        public void ManageLeftScreenCollision()
        {
            Server = Player2;
            // In classic game, the point goes to the right player
            Player2.ScoreAPoint();
            if (ResetPositionsWhenScore)
                ResetPositions();
            else
                SuperBall.Reset();
        }

        public void ManageRightScreenCollision()
        {
            Server = Player1;
            // In classic game, the point goes to the left player
            Player1.ScoreAPoint();
            if (ResetPositionsWhenScore)
                ResetPositions();
            else
                SuperBall.Reset();
        }

        private void ResetGame()
        {
            Server = Player1;
            ResetPositions();
        }

        private void ResetPositions()
        {
            Player1.Reset();
            Player2.Reset();
            SuperBall.Reset();
        }

        public override void Draw(GameTime gameTime)
        {
            Player1.Draw(gameTime);
            Player2.Draw(gameTime);
            SuperBall.Draw(gameTime);
            ScoreWriter.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
