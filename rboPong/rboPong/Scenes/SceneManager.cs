﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rboPong.Scenes
{
    public class SceneManager : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private static SceneManager Instance;
        protected RboPong PongGame { get; set; }

        private static Scene ActiveScene { get; set; }
        // private static Scene NextScene { get; set; } // if there is transition

        protected SceneManager(RboPong game)
            :base(game)
        {
            PongGame = game;
        }

        public override void Initialize()
        {
            ActiveScene = new MenuScene(PongGame);
            ActiveScene.Initialize();
            base.Initialize();
        }

        public static SceneManager getInstance(RboPong game)
        {
            if (Instance == null)
                Instance = new SceneManager(game);
           
            return Instance;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            ActiveScene.Update(gameTime);
            base.Update(gameTime);
        }

        public static void ChangeScene(Scene nextScene)
        {
            ActiveScene = nextScene;
            ActiveScene.Initialize();
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            ActiveScene.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
