﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace rboPong
{
    public abstract class AIPlayer : Player
    {
        protected float ServeInterval { get; set; }
        protected float ElapsedTimeSinceServe { get; set; }

        protected float ThinkInterval { get; set; }
        protected float ElapsedTimeSinceThink { get; set; }

        public AIPlayer(GameComponents.Match match, Vector2 initPos, Color color)
            : base(match, initPos, color)
        {
        }

        public override void Initialize()
        {
            ServeInterval = 1.0f;
            ElapsedTimeSinceServe = 0.0f;

            ThinkInterval = 0.05f;
            ElapsedTimeSinceThink = 0.0f;

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            ElapsedTimeSinceThink += elapsedTime;
            if (ElapsedTimeSinceThink >= ThinkInterval)
            {
                Think(gameTime);
                ElapsedTimeSinceThink -= ThinkInterval;
            }

            base.Update(gameTime);
        }

        protected abstract void Think(GameTime gameTime);

        public override bool isServing(GameTime gameTime)
        {
            ElapsedTimeSinceServe += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (ElapsedTimeSinceServe >= ServeInterval)
            {
                ElapsedTimeSinceServe = 0.0f;
                return true;
            }

            return false;
        }

        public override bool isActionKeyDown(GameTime gameTime)
        {
            if (Match.SuperBall.isFastSpeed())
                return Coin();
            return true;
        }


        // Flip a coin (Random bool)
        public bool Coin()
        {
            return RboPong.RandomGenerator.Next(2) == 1;
        }
    }
}
