﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace rboPong.Scenes
{
    public abstract class Scene : DrawableGameComponent
    {
        protected RboPong PongGame { get; set; }

        public Scene(RboPong game)
            : base(game)
        {
            PongGame = game;
        }

        public override void Initialize()
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
