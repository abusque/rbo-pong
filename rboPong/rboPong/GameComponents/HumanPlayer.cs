using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace rboPong
{
    /// <summary>
    /// This is the class representing a human player
    /// </summary>
    public class HumanPlayer : Player
    {
        private Keys UpKey { get; set; }
        private Keys DownKey { get; set; }
        public Keys ActionKey { get; private set; }

        public HumanPlayer(GameComponents.Match match, Vector2 initPos, Color color, Keys upKey, Keys downKey, Keys actionKey)
            : base(match, initPos, color)
        {
            UpKey = upKey;
            DownKey = downKey;
            ActionKey = actionKey;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            HandleInput();

            base.Update(gameTime);
        }

        private void HandleInput()
        {
            if (PongGame.InputManager.IsKeyDown(UpKey))
                CurrentDirection = Direction.UP;
            else if (PongGame.InputManager.IsKeyDown(DownKey))
                CurrentDirection = Direction.DOWN;
            else
                CurrentDirection = Direction.NO_DIRECTION;
        }

        public override bool isServing(GameTime gameTime)
        {
            return PongGame.InputManager.IsKeyDown(ActionKey);
        }

        public override bool isActionKeyDown(GameTime gameTime)
        {
            return PongGame.InputManager.IsNewKey(ActionKey);
        }
    }
}
