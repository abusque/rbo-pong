﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rboPong
{
    // INCOMPLETE CLASS

    public class PauseMenu : DrawableGameComponent
    {
        // <summary>
        /// The reference to the game.
        /// </summary>
        private RboPong PongGame { get; set; }

        /// <summary>
        /// The SpriteFont used to draw the score.
        /// </summary>
        private SpriteFont Font { get; set; }

        /// <summary>
        /// Local attribute used to remember position to draw.
        /// </summary>
        private Vector2 TextOrigin { get; set; }
        private Vector2 TextPosition { get; set; }
        private string Text { get; set; }

        public PauseMenu(RboPong game)
            :base(game)
        {
            PongGame = game;
        }

        public override void Initialize()
        {
            Font = ResourceManager<SpriteFont>.get("ScoreFont");

            Text = "PAUSE";
            Vector2 originToCenter = (PongGame.ScreenSize - Font.MeasureString(Text)) / 2;
            TextPosition = new Vector2(originToCenter.X, 0.75f * originToCenter.Y);
            
            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            PongGame.spriteBatch.DrawString(Font, Text, TextPosition, Color.White);
            base.Draw(gameTime);
        }
    }
}
