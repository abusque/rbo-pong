# rboPong #

rboPong is a version of the classic Pong video game developed by the Rad Bingo
Offensive team from École Polytechnique de Montréal's Poly-Games. The game is
implemented using the XNA 4.0 framework.

Rad Bingo Offensive is:

* Antoine Busque
* Cédrick Émond
* Mathieu Laprise
* Gabriel Saint-Laurent
* Valérie Vézina
