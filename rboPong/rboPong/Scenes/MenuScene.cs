﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace rboPong.Scenes
{
    public class MenuScene : Scene
    {
        private SpriteFont Font { get; set; }
        
        private string Title { get; set; }
        private Vector2 TitlePosition { get; set; }

        private List<ActionButton> Buttons { get; set; }
        private string Text { get; set; }
        private Vector2 TextPosition { get; set; }

        public MenuScene(RboPong game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            PongGame.IsMouseVisible = true;

            Font = ResourceManager<SpriteFont>.get("ScoreFont");

            Title = "RBOPONG";
            Vector2 titleSize = Font.MeasureString(Title);
            TitlePosition = new Vector2((PongGame.ScreenSize.X - titleSize.X) / 2, 0);

            Buttons = new List<ActionButton>();
            int nbButtons = 3;
            int width = (int)(PongGame.ScreenSize.X / 1.75f);
            int height = (int)(PongGame.ScreenSize.Y / 3) / nbButtons;
            int x = (int)((PongGame.ScreenSize.X - width) / 2);
            int y = (int)TitlePosition.Y + (int)titleSize.Y + height;
            Color color = Color.Red;

            AddButton(x, y, width, height, color, "Two players", new GameScene(PongGame, GameComponents.Match.Mode.TWO_PLAYERS), Keys.D1);
            y += 2 * height;
            AddButton(x, y, width, height, color, "Human Vs AI", new GameScene(PongGame, GameComponents.Match.Mode.HUMAN_VS_CRAZY_AI), Keys.D2);
            y += 2 * height;
            AddButton(x, y, width, height, color, "Human Vs Hard AI", new GameScene(PongGame, GameComponents.Match.Mode.HUMAN_VS_HARD_AI), Keys.D3);
            
            base.Initialize();
        }

        private void AddButton(int x, int y, int width, int height, Color color, String text, Scene nextScene, Keys associatedKey)
        {
            Rectangle buttonRectangle = new Rectangle(x, y, width, height);
            ActionButton newButton = new ActionButton(PongGame, buttonRectangle, Color.Red, text, Font, nextScene, associatedKey);
            newButton.Initialize();
            Buttons.Add(newButton);
        }

        public override void Update(GameTime gameTime)
        {
            if (PongGame.InputManager.IsKeyBoardActive)
            {
                if (PongGame.InputManager.IsNewKey(RboPong.QuitKey))
                    PongGame.Exit();

                for (int i = 0; i < Buttons.Count; ++i)
                {
                    if (Buttons[i].isKeyPressed())
                    {
                        Buttons[i].Click();
                        break;
                    }
                }
            }
            if (PongGame.InputManager.IsLeftClick())
            {
                for (int i = 0; i < Buttons.Count; ++i)
                {
                    if (Buttons[i].isMouseOn())
                    {
                        Buttons[i].Click();
                        break;
                    }
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            PongGame.spriteBatch.DrawString(Font, Title, TitlePosition, Color.White);

            for (int i = 0; i < Buttons.Count; ++i)
                Buttons[i].Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
