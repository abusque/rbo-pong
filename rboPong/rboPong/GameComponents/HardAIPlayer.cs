﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace rboPong.GameComponents
{
    public class HardAIPlayer : AIPlayer
    {
        protected float Epsilon { get; set; }

        public HardAIPlayer(GameComponents.Match match, Vector2 initPos, Color color)
            : base(match, initPos, color)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            Epsilon = Paddle.Collider.Height * ThinkInterval * 2;
        }

        protected override void Think(GameTime gameTime)
        {
            // TO DO : Improve the movement fluidity and the IA behavior

            float diffY;
            if (Match.SuperBall.Speed.X > 0) // Consider that AI is on right side
            {
                diffY = Match.SuperBall.Position.Y - this.Paddle.getCenter().Y;
            }
            else
            {
                diffY = PongGame.ScreenSize.Y / 2 - this.Paddle.getCenter().Y;
            }

            if (diffY < -Epsilon)
                CurrentDirection = Direction.UP;
            else if (diffY > Epsilon)
                CurrentDirection = Direction.DOWN;
            else
                CurrentDirection = Direction.NO_DIRECTION;
        }

        public override bool isServing(GameTime gameTime)
        {
            return true;
        }

        public override bool isActionKeyDown(GameTime gameTime)
        {
            return true;
        }
    }
}
