﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace rboPong
{
    public class Ball : DrawableGameComponent
    {
        static Vector2 DEFAULT_SPEED = 4.0f * Vector2.UnitX;
        static Vector2 FAST_SPEED = 2 * DEFAULT_SPEED;

        static public SpriteBatch spriteBatch;

        protected RboPong PongGame { get; set; }
        protected GameComponents.Match Match { get; private set; }

        Vector2 position;
        public Vector2 Position
        {
            get { return position; }
            private set
            {
                if (value.X <= 0)
                {
                    Match.ManageLeftScreenCollision();
                    return;
                }
                else if (value.X >= PongGame.ScreenSize.X - Match.SuperBallSize.X - 1)
                {
                    Match.ManageRightScreenCollision();
                    return;
                }
                else
                    position.X = value.X;

                if (value.Y <= 0)
                {
                    ManageUpDownScreenCollision();
                    position.Y = 0;
                }
                else if (value.Y >= PongGame.ScreenSize.Y - Match.SuperBallSize.Y - 1)
                {
                    ManageUpDownScreenCollision();
                    position.Y = PongGame.ScreenSize.Y - Match.SuperBallSize.Y - 1;
                }
                else
                    position.Y = value.Y;
            }
        }
        public Vector2 getCenter() { return new Vector2(Position.X + Texture.Width / 2, Position.Y + Texture.Height / 2); }
        private Vector2 InitialPosition { get; set; }
        private Texture2D Texture { get; set; }
        private Color Color { get; set; }
        private Vector2 speed;
        public Vector2 Speed
        {
            get { return speed; }
            private set
            {
                if (Math.Abs(value.X) > FAST_SPEED.X)
                    speed.X = FAST_SPEED.X * Math.Sign(value.X);
                else
                    speed.X = value.X;

                speed.Y = value.Y;
            }
        }
        private SoundEffect beepSound;

        private float ElapsedTimeSinceUpdate { get; set; }
        protected float UpdateInterval { get; private set; }
        int wantedFPS;
        public int WantedFPS
        {
            get { return wantedFPS; }
            protected set
            {
                wantedFPS = value;
                if (value == 0)
                {
                    wantedFPS = 1;
                }
                UpdateInterval = 1.0f / wantedFPS;
            }
        }

        private bool Served { get; set; }

        public Ball(GameComponents.Match match, Vector2 position, Color color)
            : base(match.PongGame)
        {
            PongGame = match.PongGame;
            Match = match;
            Position = position;
            Color = color;
        }

        public override void Initialize()
        {
            WantedFPS = 60;
            InitialPosition = Position;
            Speed = DEFAULT_SPEED;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            Texture = ResourceManager<Texture2D>.get("Ball");
            beepSound = ResourceManager<SoundEffect>.get("beep");

            base.LoadContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime">le temps</param>
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (!Served && Match.Server.isServing(gameTime))
                Served = true;

            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            ElapsedTimeSinceUpdate += elapsedTime;
            if (ElapsedTimeSinceUpdate >= UpdateInterval)
            {
                if (Served)
                    Move(gameTime);
                ElapsedTimeSinceUpdate -= UpdateInterval;
            }

            base.Update(gameTime);
        }

        public void Move(GameTime gameTime)
        {
            Position += Speed;
            ManageCollisions(gameTime);
        }

        #region Collisions
        private void ManageCollisions(GameTime gameTime)
        {
            Rectangle theBall = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            Rectangle paddle1 = Match.Player1.Paddle.Collider;
            Rectangle paddle2 = Match.Player2.Paddle.Collider;
            // Collision against the paddles
            // Collision with the left paddle (player 1).
            if (theBall.Intersects(paddle1) &&
                    Speed.X < 0 && // To prevent multi-collision/stuck
                    (theBall.Center.X - paddle1.Right) > 0.1f)// To prevent up/down collision

                ManagePaddleCollision(Match.Player1.isActionKeyDown(gameTime), paddle1.Center.Y, theBall.Center.Y);

            // Collision with the right paddle (player 2)
            else if (theBall.Intersects(paddle2) &&
                    Speed.X > 0 && // To prevent multi-collision/stuck
                    (paddle2.Left - theBall.Center.X) > 0.1f)// To prevent up/down collision

                ManagePaddleCollision(Match.Player2.isActionKeyDown(gameTime), paddle2.Center.Y, theBall.Center.Y);

            // Collision against the edges of the screen
            // implemented in setPosition                
        }

        private void ManagePaddleCollision(bool isActionKeyDown, int paddleY, int ballY)
        {
            float randomFactor = ((float)RboPong.RandomGenerator.NextDouble() - 0.5f);
            Rectangle theBall = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            Rectangle paddle1 = Match.Player1.Paddle.Collider;
            Rectangle paddle2 = Match.Player2.Paddle.Collider;

            // If the collision occurs in the upper part of the paddle, the ball should move up
            if (paddleY - 0.25 * paddle2.Height >= ballY)
            {
                Speed = new Vector2(-Speed.X, -2 * (randomFactor * randomFactor) - 0.5f);
            }

            // If the collision occurs in the bottom part of the paddle, the ball should move down
            else if (paddleY + 0.2 * paddle2.Height <= ballY)
            {

                Speed = new Vector2(-Speed.X, 2 * (randomFactor * randomFactor) + 0.5f);
            }

            else
            {
                Speed = new Vector2(-Speed.X, Speed.Y);
            }


            if (isActionKeyDown)
                Speed = 2 * Speed;
            else if (Math.Abs(Speed.X) > DEFAULT_SPEED.X)
                Speed = Speed / 2;

            beepSound.Play();
        }

        private void ManageUpDownScreenCollision()
        {
            Speed = new Vector2(Speed.X, -Speed.Y);
            beepSound.Play();
        }

        #endregion

        public void Reset()
        {
            Served = false;
            Position = InitialPosition;
            if (Match.Server == Match.Player1)
                Speed = DEFAULT_SPEED;
            else
                Speed = Vector2.Negate(DEFAULT_SPEED);
        }

        public bool isFastSpeed()
        {
            return speed.X == FAST_SPEED.X;
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            spriteBatch.Draw(Texture, Position, Color);
            base.Draw(gameTime);
        }

    }
}
