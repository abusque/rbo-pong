﻿/*  File : ResourceManager.cs
 *      Author : Gabriel St-Laurent
 *      DateOfCreation : 2013-10-10
 *      DateOfLastUpdate : 2013-10-10
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;

namespace rboPong
{
    /// <summary>
    /// This static class loads and unloads assets and grants access to them for the GameComponents.
    /// </summary>
    /// <typeparam name="Resource">The type of asset that this class loads.</typeparam>
    static class ResourceManager<Resource>
    {
        /// <summary>
        /// A reference to the Content. (Set by the game itself)
        /// </summary>
        static public ContentManager content;

        /// <summary>
        /// Organises the resources by name, for easy access.
        /// </summary>
        static private Dictionary<string, Resource> resources = new Dictionary<string, Resource>();

        /// <summary>
        /// Loads an asset in the manager.
        /// </summary>
        /// <param name="path">The folder where the resource is.</param>
        /// <param name="name">The name of the resource (without extension).</param>
        static public void Load(string path, string name)
        {
            resources.Add(name,content.Load<Resource>(path + name));
        }

        /// <summary>
        /// Unloads a resource by removing it from the resources.
        /// </summary>
        /// <param name="name">The name of the resource to unload.</param>
        static public void Unload(string name)
        {
            resources.Remove(name);
        }

        /// <summary>
        /// Returns the resource with that name.
        /// </summary>
        /// <param name="name">The name of the resource.</param>
        /// <returns>The resource needed.</returns>
        /// <remarks>If the resource was not loaded, an exception will be thrown.</remarks>
        static public Resource get(string name)
        {
            return resources[name];
        }
    }
}
