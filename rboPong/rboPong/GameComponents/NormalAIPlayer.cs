﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace rboPong.GameComponents
{
    public class NormalAIPlayer : AIPlayer
    {
        protected float Epsilon { get; set; }

        public NormalAIPlayer(GameComponents.Match match, Vector2 initPos, Color color)
            : base(match, initPos, color)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            ThinkInterval = 0.15f;
            Epsilon = Paddle.Collider.Height * ThinkInterval;
        }

        protected override void Think(GameTime gameTime)
        {
            // TO DO : Improve the movement fluidity and the IA behavior

            Vector2 distance = this.Paddle.getCenter() - Match.SuperBall.getCenter();
            if (distance.X < PongGame.ScreenSize.X / 2 && Match.SuperBall.Speed.X > 0) // Consider that AI is on right side
            {
                if (distance.Y > Epsilon)
                    CurrentDirection = Direction.UP;
                else if (distance.Y < -Epsilon)
                    CurrentDirection = Direction.DOWN;
                else
                    CurrentDirection = Direction.NO_DIRECTION;
            }
        }

        public override bool isServing(GameTime gameTime)
        {
            return base.isServing(gameTime);
        }

        public override bool isActionKeyDown(GameTime gameTime)
        {
            CurrentDirection = Direction.NO_DIRECTION;
            return base.isActionKeyDown(gameTime);
        }
    }
}

