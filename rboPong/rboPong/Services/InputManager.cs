﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace rboPong
{
    public class InputManager : Microsoft.Xna.Framework.GameComponent
    {
        private Game PongGame;
        private Keys[] PreviousKeys { get; set; }
        private Keys[] CurrentKeys { get; set; }
        private KeyboardState KeyBoardState { get; set; }

        private MouseState LastMouseState {get; set;}
        public Vector2 MousePosition {get {return new Vector2(Mouse.GetState().X, Mouse.GetState().Y);}}

        public InputManager(Game game)
            : base(game)
        {
            PongGame = game;
        }

        public override void Initialize()
        {
            PreviousKeys = new Keys[0];
            CurrentKeys = new Keys[0];

            LastMouseState = Mouse.GetState();
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            PreviousKeys = CurrentKeys;
            KeyBoardState = Keyboard.GetState();
            CurrentKeys = KeyBoardState.GetPressedKeys();

            LastMouseState = Mouse.GetState();

            base.Update(gameTime);
        }

        public bool IsKeyBoardActive
        {
            get { return CurrentKeys.Length > 0; }
        }

        public bool IsNewKey(Keys key)
        {
            int keyCount = PreviousKeys.Length;
            bool newKey = IsKeyDown(key);
            int i = 0;

            while (i < keyCount && newKey)
            {
                newKey = PreviousKeys[i] != key;
                ++i;
            }

            return newKey;
        }

        public bool IsKeyDown(Keys key)
        {
            return KeyBoardState.IsKeyDown(key);
        }

        public bool IsLeftClick()
        {
            bool isLeftClick = LastMouseState.LeftButton == ButtonState.Released && Mouse.GetState().LeftButton == ButtonState.Pressed;
            return isLeftClick;
        }

        public bool IsRightClick()
        {
            bool isRightClick = LastMouseState.RightButton == ButtonState.Released && Mouse.GetState().RightButton == ButtonState.Pressed;
            return isRightClick;
        }

    }
}
