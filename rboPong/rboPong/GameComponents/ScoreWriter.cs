﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rboPong
{
    /// <summary>
    /// Writes the score of the game on the upper-center of the screen.
    /// </summary>
    public class ScoreWriter : DrawableGameComponent
    {
        /// <summary>
        /// The reference to the game.
        /// </summary>
        private RboPong PongGame { get; set; }
        private GameComponents.Match Match { get; set; }

        /// <summary>
        /// The SpriteFont used to draw the score.
        /// </summary>
        private SpriteFont ScoreFont { get; set; }

        /// <summary>
        /// Local attribute used to remember position to draw.
        /// </summary>
        private Vector2 Position { get; set; }

        public ScoreWriter(GameComponents.Match match)
            :base(match.PongGame)
        {
            PongGame = match.PongGame;
            Match = match;
            Position = PongGame.ScreenSize * new Vector2(0.5f, 0);
        }

        protected override void LoadContent()
        {
            ScoreFont = ResourceManager<SpriteFont>.get("ScoreFont");
            base.LoadContent();
        }

        public override void Draw(GameTime gameTime)
        {
            //Creates the score in String form.
            string leftScore = Match.Player1.Score.ToString();
            string rightScore = Match.Player2.Score.ToString();
            string score = leftScore + " : " + rightScore;

            //Translates the position so the ":" is always on center.
            Vector2 realPosition = Position - new Vector2(ScoreFont.MeasureString(leftScore).X, 0)
                 - new Vector2(ScoreFont.MeasureString(" : ").X / 2, 0);

            //Draws the score white.
            PongGame.spriteBatch.DrawString(ScoreFont, score, realPosition, Color.White);
            
            base.Draw(gameTime);
        }
    }
}
