﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace rboPong.GameComponents
{
    class HumanVsHardAIMatch : Match
    {
        public HumanVsHardAIMatch(RboPong game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void InitializePlayers()
        {
            float yCenter = PongGame.ScreenSize.Y / 2 - PaddleSize.Y / 2;
            float player2X = PongGame.ScreenSize.X - 10 - PaddleSize.X;

            Player1 = new HumanPlayer(this, new Vector2(10, yCenter), Color.Lime, Keys.W, Keys.S, Keys.Space);
            Player2 = new HardAIPlayer(this, new Vector2(player2X, yCenter), Color.Azure);

            base.InitializePlayers();
        } 
    }
}
