﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace rboPong.Scenes
{
    public class ActionButton : DrawableGameComponent
    {
        RboPong PongGame { get; set; }

        private Vector2 Position { get; set; }
        private Rectangle Collider { get; set; }

        private String Text { get; set; }
        private Vector2 TextPosition { get; set; }
        private SpriteFont Font { get; set; }

        private Texture2D Texture { get; set; }
        public Color Color { get; set; }

        public Scene NextScene { get; private set; }
        public Keys AssociatedKey { get; private set; }

        public ActionButton(RboPong game, Rectangle positionRectangle, Color color, String text, SpriteFont font, Scene nextScene, Keys associatedKey)
            : base(game)
        {
            PongGame = game;
            Collider = positionRectangle;
            Position = new Vector2(Collider.Location.X, Collider.Location.Y);
            Color = color;
            Text = text;
            Font = font;
            NextScene = nextScene;
            AssociatedKey = associatedKey;
        }

        public override void Initialize()
        {
            Texture = ResourceManager<Texture2D>.get("Paddle");
            Vector2 fontSize = Font.MeasureString(Text);
            int x = Collider.X + (int)(Collider.Width - fontSize.X) / 2;
            int y = Collider.Y + (int)(Collider.Height - fontSize.Y) / 2;
            TextPosition = new Vector2(x, y);
            base.Initialize();
        }

        public bool isMouseOn()
        {
            Vector2 mousePosition = PongGame.InputManager.MousePosition;
            return Collider.Contains((int)mousePosition.X, (int)mousePosition.Y);
        }

        public bool isKeyPressed()
        {
            return PongGame.InputManager.IsNewKey(AssociatedKey);
        }

        public bool isSelected()
        {
            return isKeyPressed() || (PongGame.InputManager.IsLeftClick() && isMouseOn());
        }

        public void Click()
        {
            SceneManager.ChangeScene(NextScene);
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            PongGame.spriteBatch.Draw(Texture, Position, Color);
            PongGame.spriteBatch.DrawString(Font, Text, TextPosition, Color.White);
            base.Draw(gameTime);
        }
    }
}
