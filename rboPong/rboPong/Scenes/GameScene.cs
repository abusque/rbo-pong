﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace rboPong.Scenes
{
    public class GameScene : Scene
    {
        private GameComponents.Match Match { get; set; }
        private PauseMenu PauseMenu { get; set; }
        private GameComponents.Match.Mode Mode {get; set;}

        private bool isPaused;

        public GameScene(RboPong game, GameComponents.Match.Mode mode)
            : base(game)
        {
            Mode = mode;
        }

        public override void Initialize()
        {
            PongGame.IsMouseVisible = false;

            Match = GameComponents.Match.getMatch(PongGame, Mode);
            Match.Initialize();

            PauseMenu = new PauseMenu(PongGame);
            PauseMenu.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (PongGame.InputManager.IsKeyBoardActive)
            {
                if (PongGame.InputManager.IsNewKey(RboPong.QuitKey))
                    SceneManager.ChangeScene(new MenuScene(PongGame));

                if (PongGame.InputManager.IsNewKey(RboPong.PauseKey))
                    isPaused = !isPaused;
            }

            if(!isPaused)
                Match.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Match.Draw(gameTime);

            if (isPaused)
                PauseMenu.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
