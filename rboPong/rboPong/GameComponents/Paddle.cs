﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace rboPong
{
    public enum Direction { NO_DIRECTION, UP, DOWN, NB_DIRECTION }

    public class Paddle : DrawableGameComponent
    {
        public const float DEFAULT_SPEED = 2.0f;
        static public SpriteBatch spriteBatch;

        // Top-left corner of the paddle
        private Vector2 position;
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                if (Texture != null)
                {
                    if (value.Y < 0)
                        value.Y = 0;
                    else if (value.Y + Texture.Height > Game.GraphicsDevice.Viewport.Height)
                        value.Y = Game.GraphicsDevice.Viewport.Height - Texture.Height;
                }

                position = value;
            }
        }
        public Vector2 getCenter() { return new Vector2(Collider.Center.X, Collider.Center.Y); }
        public float Speed { get; private set; }
        private float InitialSpeed { get; set; }
        private float Acceleration { get; set; }
        private Direction LastDirection { get; set; }


        private Vector2 InitialPosition { get; set; }
        private Texture2D Texture { get; set; }
        private Color Color { get; set; }

        public Rectangle Collider
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            }
        }

        public Paddle(Game game, Vector2 position, Color color)
            :base(game)
        {
            Position = position;
            Color = color;
        }

        public override void Initialize()
        {
            InitialPosition = Position;
            InitialSpeed = Player.DEFAULT_UPDATE_INTERVAL / Player.UpdateInterval * DEFAULT_SPEED;
            Speed = InitialSpeed;
            Acceleration = 0.15f * Speed;
            LastDirection = Direction.NO_DIRECTION;
            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            Texture = ResourceManager<Texture2D>.get("Paddle");

            base.LoadContent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTime">le temps</param>
        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public void Move(Direction direction)
        {
            if (LastDirection == direction)
            {
                Speed += Acceleration;
            }
            else
            {
                Speed = InitialSpeed;
                LastDirection = direction;
            }

            if (direction == Direction.UP)
                Position = new Vector2(Position.X, Position.Y - Speed);
            else if (direction == Direction.DOWN)
                Position = new Vector2(Position.X, Position.Y + Speed);
        }

        public void Reset()
        {
            Position = InitialPosition;
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            spriteBatch.Draw(Texture, Position, Color);
            base.Draw(gameTime);
        }
    }
}
