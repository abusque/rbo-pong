﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace rboPong
{
    public abstract class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public static float DEFAULT_UPDATE_INTERVAL {get; private set;}

        protected RboPong PongGame { get; private set; }
        protected GameComponents.Match Match { get; private set; }
        public Paddle Paddle { get; private set; }
        public uint Score { get; private set; }

        protected float ElapsedTimeSinceUpdate { get; set; }
        public static float UpdateInterval { get; private set; }

        protected Direction CurrentDirection { get; set; } 

        static Player()
        {
            DEFAULT_UPDATE_INTERVAL = 1.0f / 100; 
        }
        
        public Player(GameComponents.Match match, Vector2 initPos, Color color)
            : base(match.PongGame)
        {
            Match = match;
            PongGame = match.PongGame;
            Paddle = new Paddle(match.PongGame, initPos, color);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            UpdateInterval = DEFAULT_UPDATE_INTERVAL;

            Paddle.Initialize();
            Score = 0;

            CurrentDirection = Direction.NO_DIRECTION;

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            ElapsedTimeSinceUpdate += elapsedTime;
            if (ElapsedTimeSinceUpdate >= UpdateInterval)
            {
                Paddle.Move(CurrentDirection);
                Paddle.Update(gameTime);
                ElapsedTimeSinceUpdate -= UpdateInterval;
            }

            base.Update(gameTime);
        }

        public void ScoreAPoint()
        {
            ++Score;
        }

        public void ResetScore()
        {
            Score = 0;
        }

        public void Reset()
        {
            ResetScore();
            Paddle.Reset();
        }

        public abstract bool isServing(GameTime gameTime);

        public abstract bool isActionKeyDown(GameTime gameTime);

        public override void Draw(GameTime gameTime)
        {
            Paddle.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
